# Analysis of unstructured data

University course **Analysis of unstructured data** repository

### Author: Jarosław Opała

# Lab assignment 1

The main goal of the task was to analyse a chosen dataset. The dataset used in the report was downloaded from https://www.kaggle.com/stefanoleone992/fifa-20-complete-player-dataset. Data consist of information about players in FIFA game since 2015 until 2020. Only FIFA20 dataset was selected for analysis. 

# Files
- **lab1.ipynb** - Jupyter Notebook report
- **data/players.csv** - dataset